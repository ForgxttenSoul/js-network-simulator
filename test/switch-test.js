var s1=Switch();
var s2=Switch();
var s3=Switch();
var s4=Switch();
var s5=Switch();

s1.connect(s2);
s2.connect(s3);
s3.connect(s4);
s4.connect(s5);

var n1=Nic({
    mac:"11"
});
var n2=Nic({
    mac:"22"
});
var n3=Nic({
    mac:"33"
});
var n4=Nic({
    mac:"44"
});

n1.connect(s1);
n2.connect(s2);
n3.connect(s3);
n4.connect(s5);

var t=[n1,n2,n3,n4];

t.forEach(function(ni,index){
    ni.on_data(function(packet){
        if(packet.source===ni.getMac()){
            return false;
        }
        if(packet.target===ni.getMac()||packet.target==="*"){
            if(packet.data.command==="!ping"){
                ni.send(packet.source,{
                    command:"!pong"
                });
            }
            if(packet.data.command==="!pong"){
                console.log(ni.getMac(),":",packet.source,"responded to !ping");
            }
        }
    });
});

n1.send("*",{
    command:"!ping"
});
n2.send("*",{
    command:"!ping"
});
n3.send("*",{
    command:"!ping"
});
n4.send("*",{
    command:"!ping"
});