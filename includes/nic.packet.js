nic_packet=(function(IN){
    if(IN.getType()!=="nic"){
        throw new Error("nic supported only");
    }
    var nic=IN;
    var mac=_mac_();
    var on_packet=(function(packet){});
    nic.on_packet=(function(IN){
        on_packet=IN;
    });
    nic.on_data(function(packet){
        if(packet.target==="*"||packet.target===nic.getMac()){
            on_packet(packet);
        }
    });
    nic.send_packet=(function(target,data){
        var packet={
            target:target,
            source:nic.getMac(),
            data:data,
            nonce:_nonce_()
        };
        nic.send(packet);
    });
});