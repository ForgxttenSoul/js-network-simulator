emitter=(function(){
    var ee={};
    var _ee={};
    _ee.clone=(function(IN){
        return JSON.parse(JSON.stingify(IN),true);
    });
    _ee.events={};
    _ee.ids=[];
    _ee.new_id=(function(){
        var id=Math.round(new Date().getTime()*Math.random());
        while(_ee.ids.indexOf(id)!==-1){
            id=Math.round(new Date().getTime()*Math.random());
        }
        return id;
    });
    ee.on=(function(event,call){
        var id=_ee.new_id();
        _ee.events[id]={
            event:event,
            callback:call
        };
        return id;
    });
    ee.emit=(function(event,data){
        var events=Object.keys(_ee.events);
        events.forEach(function(id,num){
            var _event=_ee.events[id];
            if(_event.event===event){
                setTimeout(function(){
                    _event.callback(_ee.clone(data));
                },0);
            }
        });
    });
    ee.cancel=(function(id){
        try{
            delete _ee.events[id];
        }catch(e){}
    });
    return ee;
});