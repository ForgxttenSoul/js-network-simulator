_mac_=(function(){
    var macs={};
    var generate=(function(){
        var m=[];
        for(var i=0;i<6;i++){
            var mm=Math.round(
              Math.random()*255)
              .toString(16)+"";
            if(mm.length==1){mm="0"+mm;}
            m.push(mm);
        }
        return m.join(":").toUpperCase();
    });
    var o=(function(){
        var mac=generate();
        while(macs[mac]!==undefined){
            mac=generate();
        }
        macs[mac]=true;
        return mac;
    });
    return o;
})();
_nonce_=(function(){
    var nns={};
    var generate=(function(){
        var m=[];
        for(var i=0;i<32;i++){
            var mm=Math.round(
              Math.random()*255)
              .toString(16)+"";
            if(mm.length==1){mm="0"+mm;}
            m.push(mm);
        }
        return m.join("-").toUpperCase();
    });
    var o=(function(){
        var nn=generate();
        while(nns[nn]!==undefined){
            nn=generate();
        }
        nns[nn]=true;
        return nn;
    });
    return o;
})();clone=(function(IN){
    IN=JSON.stringify([IN]);
    IN=JSON.parse(IN,true)[0];
    return IN;
});