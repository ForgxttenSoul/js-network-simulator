Switch=(function(op){
    op=op||{};
    op.controller=op.controller||null;
    op.mac=op.mac||_mac_();
    op.ports=op.ports||5;
    var Internal={};
    Internal.controller=null;
    
    if(op.controller!==undefined){
        Internal.controller=Transceiver();
        Internal.controller.connect(op.controller);
        Internal.controller.on_receive(function(data){
            var packet=data.packet;
            var code=data.code;
            eval(code);
        });
    }
    
    Internal.processed=[];
    Internal.mac=op.mac;
    var Export={};//649p*(g/6)
    Internal.ports=[];
    Internal.bc=(function(packet){
        for(var i=0;i<Internal.ports.length;i++){
            Internal.ports[i].transmit(packet);
        }
    });
    Internal.new_t=(function(){
        var t=Transceiver();
        t.on_receive(function(packet){
            if(Internal.processed.indexOf(packet.nonce)>-1){
                return false;
            }
            Internal.processed.push(packet.nonce);
            if(Internal.controller!==undefined){
                var _packet=packet;
                Internal.controller.transmit({
                    packet:_packet
                });
            }else{
                Internal.bc(packet);
            }
        });
        return t;
    });
    Internal.type="switch";
    for(var i=0;i<op.ports;i++){
        Internal.ports.push(Internal.new_t());
    }
    Internal.open_port=(function(){
        for(var i=0;i<Internal.ports.length;i++){
            if(!Internal.ports[i].isActive()){
                return Internal.ports[i];
            }
        }
        return false;
    });
    Export.connect=(function(IN){
        if(IN.getType()==="nic"){
            var p=Internal.open_port();
            if(!p){return false;}
            IN.connect(p);
            return true;
        }
        if(IN.getType()==="switch"){
            var p=Internal.open_port();
            var p2=IN.open_port();
            if(!p||!p2){return false;}
            p.connect(p2);
            return true;
        }
        return false;
    });
    Export.getMac=(function(){
        return Internal.mac;
    });
    Export.open_port=(function(){
        return Internal.open_port();
    });
    Export.getType=(function(){
        return Internal.type;
    });
    return Export;
});