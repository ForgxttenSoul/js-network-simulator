Nic=(function(ops={}){
    ops.mac=ops.mac||_mac_();
    var Internal={};
    var Exports={};
    Internal.mac=ops.mac;
    Internal.type="nic";
    Internal.who=null;
    Internal.processed=[];
    Internal.transceiver=Transceiver();
    Internal.on_data=(function(IN){});
    Internal.transceiver.on_receive(function(packet){
        if(Internal.processed.indexOf(packet.nonce)>-1){
            return false;
        }
        Internal.processed.push(packet.nonce);
        Internal.on_data(packet);
    });
    Exports.getType=(function(){
        return Internal.type;
    });
    Exports.getMac=(function(){
        return Internal.mac;
    });
    Exports.transceiver=(function(){
        return Internal.transceiver;
    });
    Exports.on_data=(function(IN){
        Internal.on_data=IN;
        return true;
    });
    Exports.connected=(function(){
        return (Internal.transceiver.isActive());
    });
    Exports.send=(function(target,data){
        if(Internal.who===null){return false;}
        if(Exports.connected()){
            if(["switch","nic"].indexOf(Internal.who)>-1){
                var packet={
                    target:target,
                    source:Exports.getMac(),
                    data:data,
                    nonce:_nonce_()
                };
            }else{
                var packet=target;
            }
            return Internal.transceiver.transmit(packet);
        }
    });
    Exports.disconnect=(function(){
        Internal.who=null;
        Internal.transceiver.disconnect;
    });
    Exports.connect=(function(IN){
        if(IN.getType()==="switch"){
            Internal.who=IN.getType();
            return Internal.transceiver.connect(IN.open_port());
        }
        if(IN.getType()==="nic"){
            Internal.who=IN.getType();
            return IN.transceiver.connect(Internal.transceiver);
        }
        if(IN.getType()==="transceiver"){
            Internal.who=IN.getType();
            return IN.connect(Internal.transceiver);
        }
        return false;
    });
    return Exports;
});