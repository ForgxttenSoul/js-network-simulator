waves=emitter();
Radio=(function(){
    var Internal={};
    var Exports={};
    Internal.receive=(function(IN){});
    Internal.channel=0;
    Internal.wave_id=waves.on(`channel:${Internal.channel}`,function(IN){
        Internal.receive(IN);
    });
    Exports.tune=(function(IN){
        waves.cancel(Internal.wave_id);
        Internal.wave_id=waves
          .on(`channel:${Internal.channel}`,
          function(IN){
            Internal.receive(IN);
        });
    });
    Exports.receive=(function(IN){
        Internal.receive=IN;
    });
    Exports.transmit=(function(IN){
        waves.emit(`channel:${Internal.channel}`,IN);
    });
    return Exports;
});