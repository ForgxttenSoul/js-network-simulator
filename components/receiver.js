Receiver=(function(){
    var Export={};
    var Internal={};
    Internal.transmitter=null;
    Internal.active=false;
    Internal.type="receiver";
    Internal.receive=(function(IN){});
    Export.getType=(function(){
        return Internal.type;
    });
    Export.attach=(function(IN){
        if(Internal.active===true){
            return false;
        }
        if(IN.getType()!=="transmitter"){
            return false;
        }
        Internal.active=true;
        Internal.transmitter=IN;
        Internal.transmitter.attach(Export);
        return true;
    });
    Export.detach=(function(){
        if(Internal.active===false){
            return false;
        }
        Internal.active=false;
        Internal.transmitter.detach();
        Internal.transmitter=null;
        return true;
    });
    Export.on_receive=(function(fun){
        Internal.receive=fun;
    });
    Export.receive=(function(obj){
        Internal.receive(obj);
    });
    Export.isActive=(function(){
        return Internal.active;
    });
    return Export;
});