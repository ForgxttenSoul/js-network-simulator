Transmitter=(function(){
    var Export={};
    var Internal={};
    Internal.receiver=null;
    Internal.active=false;
    Internal.type="transmitter";
    Export.getType=(function(){
        return Internal.type;
    });
    Export.attach=(function(IN){
        if(Internal.active===true){
            return false;
        }
        if(IN.getType()!=="receiver"){
            return false;
        }
        Internal.active=true;
        Internal.receiver=IN;
        Internal.receiver.attach(Export);
        return true;
    });
    Export.detach=(function(){
        if(Internal.active===false){
            return false;
        }
        Internal.active=false;
        Internal.receiver.detach();
        Internal.receiver=null;
        return true;
    });
    Export.transmit=(function(obj){
        if(Export.isActive()){
            setTimeout(function(){
                try{
                    Internal
                      .receiver
                      .receive(clone(obj));
                }catch(e){
                    console.log(e);
                }
            },0);
            return true;
        }
        return false;
    });
    Export.isActive=(function(){
        return Internal.active;
    });
    return Export;
});