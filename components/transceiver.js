Transceiver=(function(){
    var Export={};
    var Internal={};
    var processed=[];
    Internal.IN=Receiver();
    Internal.OUT=Transmitter();
    Internal.mac=_mac_();
    Internal.type="transceiver";
    Internal.active=false;
    Internal.receive=(function(IN){});
    Internal.IN.on_receive(function(IN){
        Internal.receive(IN);
    });
    Export.getMac=(function(){
        return Internal.mac;
    });
    Export.get={
        IN:(function(){
            return Internal.IN;
        }),
        OUT:(function(){
            return Internal.OUT;
        })
    };
    Export.getType=(function(){
        return Internal.type;
    });
    Export.transmit=(function(OUT){
        return Internal.OUT.transmit(OUT);
    });
    Export.on_receive=(function(fun){
        Internal.receive=fun;
        return true;
    });
    Export.isActive=(function(){
        return Internal.OUT.isActive();
    });
    Export.connect=(function(IN){
        if(Export.isActive()){
            return false;
        }
        if(IN.getType()!=="transceiver"){
            return false;
        }
        Internal.IN.attach(IN.get.OUT());
        Internal.OUT.attach(IN.get.IN());
        return true;
    });
    Export.disconnect=(function(){
        if(!Export.isActive()){
            return false;
        }
        Internal.IN.detach();
        Internal.OUT.detach();
        return true;
    });
    return Export;
});