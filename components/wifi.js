wifi=wifi||{};

wifi.router=(function(ops={}){
    ops.ssid=ops.ssid||_nonce_();
    ops.mac=ops.mac||_mac_();
    var nic=Nic({
        mac:ops.mac
    });
    var channel=`wifi.1`;//wifi.[1,2,3]
    var mac=nic.getMac();
    var ssid=ops.ssid;
    var security=false;
    var security_password=null;
    var hide_ssid=false;
    var antenna=Radio();
    var $switch=Switch();
    var external=Transceiver();
    $switch.connect(external);
    antenna.tune(channel);
    antenna.receive(function(data){
        if([data.target,"*"].indexOf(mac)>-1){
            if(data.command==="scan"){
                antenna.transmit({
                    target:data.mac,
                    command:"rescan",
                    ssid:(hide_ssid?false:ssid),
                    mac:mac,
                    channel:channel,
                    security:security
                });
            }
        }
    });
    return {
        connect:{
            external:(function(IN){
                return IN.connect(external);
            },
            local:(function(IN){
                return $switch.connect(IN);
            }
        },
        ssid:(function(IN){
            if(IN===undefined){
                return ssid;
            }else{
                ssid=IN;
            }
        },
        hide_ssid:(function(IN){
            if(IN===unefined){
                return hide_ssid;
            }else{
                hide_ssid=IN;
            }
        }),
        nic:nic
    };
});